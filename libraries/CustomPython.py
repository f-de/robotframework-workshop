from robot.api import logger
import re

def return_answer(arg):
    logger.info('example')
    return 42

def concat_two_strings(s1, s2):
    return s1 + '|' + s2

def count_words(filename):
    number_of_words = 0
    with open(filename) as file:
        lines = file.readlines()
        for line in lines:
            number_of_words += len(re.split(' |\||;|,', line))
    return number_of_words
