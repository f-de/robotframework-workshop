*** Settings ***
Documentation  Example of how to extend Robot Framework by your own python keyword library
...            and generating documentation for libraries. 
Library  CustomPython.py
Library  String


*** Test Cases ***
Example
  ${answer}=  Return Answer    arg
  Log  ${answer}

Exercise 1a
  Log To Console    In this test case you should join two string using a custom Python keyword.
  ${concat_two_strings}=    concat two strings    string1    string2
  Log To Console    ${concat_two_strings}
  Should Be Equal    ${concat_two_strings}    string1|string2

Exercise 1b
  Log To Console    In this test case you should count the number of words in a file using a custom Python keyword.
  ${result}=    Count Words    C:\\Users\\fdemuth\\AppData\\Local\\Temp\\customers.csv
  Log To Console    count_words result : ${result}
  Should Be Equal As Integers    9    ${result}