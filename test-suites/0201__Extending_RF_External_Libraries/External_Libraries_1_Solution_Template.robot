*** Settings ***
Documentation  Example for using an external database library,
...            writing a test suite for an ETL job and using variable files.
Library  DatabaseLibrary
Library  OperatingSystem


*** Variables ***
${schema_name}     workshop


*** Test Cases ***
Example
  Log Variables
  Connect To Database    dbapiModuleName=psycopg2    dbName=${dbName}    dbUsername=${dbUsername}    dbPassword=${dbPassword}    dbHost=${dbHost}    dbPort=${dbPort}
  Run Keyword And Ignore Error    Execute Sql String    DROP SCHEMA ${schema_name} CASCADE
  Execute Sql String    CREATE SCHEMA ${schema_name}
  ${res}=    Query    SELECT schema_name FROM information_schema.schemata where schema_name = '${schema_name}'
  Log  ${res}[0]
  Should Be Equal    ${schema_name}    ${res}[0][0]
  Disconnect From Database

Database Test
  Log To Console    In this test case you should verify the table content.
  Prepare Database
  Connect To Database    dbapiModuleName=psycopg2    dbName=${dbName}    dbUsername=${dbUsername}    dbPassword=${dbPassword}    dbHost=${dbHost}    dbPort=${dbPort}
  ${queryResults}=    Query    SELECT * FROM ${schema_name}.mytest
  Log To Console    ${queryResults}
  Should Be Equal As Strings    ${queryResults}    [(1, 'the surname', 'the state')]
  Disconnect From Database

Check Variables
  Log To Console    In this test case you should verify that variables are loaded correctly.

Load Data To Stage
  Log To Console    In this test case you should run the Talend job and verify that the job wrote data to the target table.


*** Keywords ***
Prepare Database
  Log To Console    This keyword should setup the database.
  Connect To Database    dbapiModuleName=psycopg2    dbName=${dbName}    dbUsername=${dbUsername}    dbPassword=${dbPassword}    dbHost=${dbHost}    dbPort=${dbPort}
  Execute Sql String    DROP TABLE IF EXISTS ${schema_name}.mytest
  Execute Sql String    CREATE TABLE ${schema_name}.mytest(id integer, surname varchar, state varchar)
  Execute Sql String    INSERT INTO ${schema_name}.mytest(id, surname, state) VALUES(1, 'the surname', 'the state')
  Disconnect From Database


Run Talend Job With Parameters Succesfully
  Log To Console    This keyword should run the Talend job and check the return code.