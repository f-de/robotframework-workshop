*** Settings ***
Documentation  Basic examples with Robot Framework data types, libraries, error handling and logging.
Library  Collections


*** Variables ***
&{mountain_elevation}=  Brocken=1141  Zugspitze=2962
@{list}  1  2  3  4  5  6  7  42
@{list2}  1  2  3  4  5  99  7  42
${seven_number}  ${7}
${seven_string}  7
&{city_population}=  Berlin=3.8  Hamburg=1.9



*** Test Cases ***
Example: Numbers
  Should Not Be Equal  ${seven_number}  7
  Should Be Equal  ${seven_string}  7
  Should Not Be Equal  ${seven_string}  ${seven_number}
  Should Be Equal As Integers  ${seven_number}  7
  ${seven_number}=  Convert To String  ${seven_number}
  Should Be Equal  ${seven_number}  7

Example: Dictionary - Log Mountain Elevation
  Log Many  &{mountain_elevation}
  Log  ${mountain_elevation}[Brocken]
  Log To Console  ${mountain_elevation}[Zugspitze]

Log variable data types
  Log To Console    In this test case you should log the data types of the variables in the variables section.
  ${type}=    Evaluate    type(${mountain_elevation})
  Log To Console    Dict type: ${type}
  ${type}=    Evaluate    type(${seven_number})
  Log To Console    Number type: ${type}
  ${type}=    Evaluate    type(${list})
  Log To Console    List type: ${type}
  ${type}=    Evaluate    type(${seven_string}).__name__

Working with lists
  Log To Console    In this test case you should compare two list variables.
  ${status}=    Run Keyword And Return Status    Lists Should Be Equal    ${list}    ${list2}
  Should Be Equal As Strings    False    ${status}
  

Working with dictionaries
  Log To Console    In this test case you should do some dictionary operations.
  Dictionary Should Not Contain Key    ${mountain_elevation}    Watzmann
  ${german_facts}  Create Dictionary  mountain_elevation=${mountain_elevation}  city_population=${city_population}
  ${h_pop}=    Evaluate     ${german_facts}[city_population][Hamburg]
  Log To Console    Hamburg population: ${h_pop}