*** Settings ***
Documentation  Basic examples with file handling and for loop in Robot Framework.
Library  OperatingSystem
Library  String
Library    Collections


*** Variables ***
${filename}  customers.csv
${expected_content}    SEPARATOR=\n
...             Mueller|42|Hamburg
...             Meier|67|Muenchen
...             Wuensch|38|Koeln


*** Test Cases ***
Example: create and move file
   Create File  ${filename}  ${expected_content}
   Move File    ${filename}  ${TEMPDIR}/${filename}
   Directory Should Not Be Empty    ${TEMPDIR}
   #Remove File    ${TEMPDIR}/${filename}

Customer File Existence
   Log To Console    In this test case you should check whether the file exists.
   File Should Exist    C:\\Users\\fdemuth\\AppData\\Local\\Temp\\customers.csv

File Content Should Be As Expected
    Log To Console    In this test case you should check whether the content of the file is correct.
    ${file_content}=    Get File    ${TEMPDIR}/${filename}
    Log To Console    ${file_content}
    Should Be Equal    Mueller|42|Hamburg\nMeier|67|Muenchen\nWuensch|38|Koeln    ${file_content}


Loop through file content
  Log To Console    In this test case you should split the file content into single fields.
  ${file_content}=    Get File    ${TEMPDIR}/${filename}
  @{lines}= 	Split To Lines    ${file_content}
  @{split_lines}=    Create List    @{EMPTY}
  FOR    ${line}    IN    @{lines}
      Log To Console    line: ${line}
      @{split_line}= 	Split String    ${line}
      Log To Console    split_line: @{split_line}
      Append To List    ${split_lines}    ${split_line}
  END